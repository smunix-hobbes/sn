let
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          sn = haskellPackagesNew.callPackage ./sn.nix {};
          dunai = haskellPackagesNew.callPackage ./dunai.nix {};
          simple-affine-space = haskellPackagesNew.callPackage ./simple-affine-space.nix {};
          rhine = haskellPackagesNew.callPackage ../rhine/rhine/rhine.nix {};
        };
      };
    };
  };
  nixpkgs = import <nixpkgs> { inherit config; };
in
{
  sn = nixpkgs.haskellPackages.sn;
}
