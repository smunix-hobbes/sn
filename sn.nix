{ mkDerivation, base, bytestring, dunai, hpack, hspec
, optparse-simple, rhine, rio, simple-affine-space, stdenv
}:
mkDerivation {
  pname = "sn";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring dunai rhine rio simple-affine-space
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base bytestring dunai optparse-simple rhine rio simple-affine-space
  ];
  testHaskellDepends = [
    base bytestring dunai hspec rhine rio simple-affine-space
  ];
  prePatch = "hpack";
  homepage = "https://github.com/githubuser/sn#readme";
  license = stdenv.lib.licenses.bsd3;
}
