{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.Prim ( Type(..)
                  , Types.Prim.bool, i8, u8, i16, u16, i32, u32, f32, i64, u64, f64
                  , Store(..)
                  , ComputeSize(..)
                  , Field(..)
                  , Write(..)
                  , eval
                  , exec
                  , addfield
                  , struct
                  , align
                  ) where

import Import
import qualified Data.ByteString.Builder as B
import qualified Data.Kind as Kind
import GHC.TypeLits (Symbol)

data ComputeSize a where
  ComputeSize :: { computeSize :: Int -> Int -> (Int, Int, a) } ->  ComputeSize a

instance Functor ComputeSize where
  fmap f ComputeSize{..} = ComputeSize { computeSize = outFn, .. }
    where
      outFn p a = fr `seq` (p, a, fr)
        where
          (_, _, r) = computeSize p a
          fr = f r

instance Applicative ComputeSize where
  pure x = ComputeSize (\p a -> (p, a, x))
  ComputeSize f <*> ComputeSize x = ComputeSize{..}
    where
      computeSize p a = seq fnv (p, a, fnv)
        where
          (_, _, fn) = f p a
          (_, _, v) = x p a
          fnv = fn v

instance Monad ComputeSize where
  return = pure
  ComputeSize{..} >>= f = ComputeSize { computeSize = outFn, .. }
    where
      outFn p a = p'' `seq` a''' `seq` r' `seq` (p'', a''', r')
        where
          (p', a', r) = computeSize p a
          (p'', a'', r') = exec p' (f r)
          a''' = max a' a''

exec :: Int -> ComputeSize a -> (Int, Int, a)
exec p ComputeSize{..} = seq r r
  where
    r = computeSize p 0

eval :: Int -> ComputeSize a -> a
eval p m = seq r r
  where
    (_, _, r) = exec p m

align :: Int -> Int -> Int
align p a | 0 == a = p
          | otherwise = seq n n
  where
    (q, r) = divMod p a
    n | 0 == r = p
      | otherwise = (1+q)*a

addfield :: forall t . (Store t) => ComputeSize ()
addfield = addfield' Proxy
  where
    addfield' :: forall proxy . proxy t -> ComputeSize ()
    addfield' field = do
      let
        (_, _, sizeAd) = exec 0 (sizeOf field)
        (_, _, alignAd) = exec 0 (alignOf field)
        computeSize p a = p' `seq` a' `seq `(p', a', ())
          where
            p' = align p alignAd + sizeAd
            a' = max a alignAd
      ComputeSize{..}

struct :: ComputeSize m -> ComputeSize m
struct m = ComputeSize \p' a' ->
                         let
                           { a'' = max a' a;
                             p'' = align (align p' a + p) a''
                           }
                         in
                           p'' `seq` a'' `seq` (p'', a'', r)
  where
    (p, a, r) = exec 0 m

data Type where
  TyB8  :: Type
  TyI8  :: Type
  TyU8  :: Type
  TyI16 :: Type
  TyU16 :: Type
  TyI32 :: Type
  TyU32 :: Type
  TyF32 :: Type
  TyI64 :: Type
  TyU64 :: Type
  TyF64 :: Type
  deriving (Show, Enum, Eq)

-- | convert symbols into types
class PrimTy (t :: Symbol) where
  type family PrimTyOf t :: Types.Prim.Type

instance PrimTy "bool" where
  type PrimTyOf "bool" = TyB8

instance PrimTy "i8" where
  type PrimTyOf "i8" = TyI8

instance PrimTy "u8" where
  type PrimTyOf "u8" = TyU8

instance PrimTy "i16" where
  type PrimTyOf "i16" = TyI16

instance PrimTy "u16" where
  type PrimTyOf "u16" = TyU16

instance PrimTy "i32" where
  type PrimTyOf "i32" = TyI32

instance PrimTy "u32" where
  type PrimTyOf "u32" = TyU32

instance PrimTy "f32" where
  type PrimTyOf "f32" = TyF32

instance PrimTy "i64" where
  type PrimTyOf "i64" = TyI64

instance PrimTy "u64" where
  type PrimTyOf "u64" = TyU64

instance PrimTy "f64" where
  type PrimTyOf "f64" = TyF64

-- | Store
class Store (t :: Symbol) where
  data family Field (fn :: Symbol) t :: Kind.Type
  type family Ground t :: Kind.Type
  store :: Ground t -> Field fn t
  sizeOf :: proxy t -> ComputeSize Int
  alignOf :: proxy t -> ComputeSize Int

sizeHelper:: Int -> ComputeSize Int
sizeHelper n = ComputeSize \p a -> (align p n + n, max n a, align 0 n + n)

instance Store "bool" where
  type Ground "bool" = Bool
  data Field fn "bool" where
    B8 :: Ground "bool" -> Field fn "bool"
    deriving (Show)
  store = B8
  sizeOf _ = sizeHelper 1
  alignOf _ = sizeOf @"bool" Proxy

instance Store "i8" where
  type Ground "i8" = Char
  data Field fn "i8" where
    I8 :: Ground "i8" -> Field fn "i8"
    deriving (Show)
  store = I8
  sizeOf _ = sizeHelper 1
  alignOf _ = sizeOf @"i8" Proxy

instance Store "u8" where
  type Ground "u8" = Word8
  data Field fn "u8" where
    U8 :: Ground "u8" -> Field fn "u8"
    deriving (Show)
  store = U8
  sizeOf _ = sizeHelper 1
  alignOf _ = sizeOf @"u8" Proxy

instance Store "i16" where
  type Ground "i16" = Int16
  data Field fn "i16" where
    I16 :: Ground "i16" -> Field fn "i16"
    deriving (Show)
  store = I16
  sizeOf _ = sizeHelper 2
  alignOf _ = sizeOf @"i16" Proxy

instance Store "u16" where
  type Ground "u16" = Word16
  data Field fn "u16" where
    U16 :: Ground "u16" -> Field fn "u16"
    deriving (Show)
  store = U16
  sizeOf _ = sizeHelper 2
  alignOf _ = sizeOf @"u16" Proxy

instance Store "i32" where
  type Ground "i32" = Int32
  data Field fn "i32" where
    I32 :: Ground "i32" -> Field fn "i32"
    deriving (Show)
  store = I32
  sizeOf _ = sizeHelper 4
  alignOf _ = sizeOf @"i32" Proxy

instance Store "u32" where
  type Ground "u32" = Word32
  data Field fn "u32" where
    U32 :: Ground "u32" -> Field fn "u32"
    deriving (Show)
  store = U32
  sizeOf _ = sizeHelper 4
  alignOf _ = sizeOf @"i32" Proxy

instance Store "f32" where
  type Ground "f32" = Float
  data Field fn "f32" where
    F32 :: Ground "f32" -> Field fn "f32"
    deriving (Show)
  store = F32
  sizeOf _ = sizeHelper 4
  alignOf _ = sizeOf @"f32" Proxy

instance Store "u64" where
  type Ground "u64" = Word64
  data Field fn "u64" where
    U64 :: Ground "u64" -> Field fn "u64"
    deriving (Show)
  store = U64
  sizeOf _ = sizeHelper 8
  alignOf _ = sizeOf @"u64" Proxy

instance Store "i64" where
  type Ground "i64" = Int64
  data Field fn "i64" where
    I64 :: Ground "i64" -> Field fn "i64"
    deriving (Show)
  store = I64
  sizeOf _ = sizeHelper 8
  alignOf _ = sizeOf @"i64" Proxy

instance Store "f64" where
  type Ground "f64" = Double
  data Field fn "f64" where
    F64 :: Ground "f64" -> Field fn "f64"
    deriving (Show)
  store = F64
  sizeOf _ = sizeHelper 8
  alignOf _ = sizeOf @"f64" Proxy

bool :: Ground "bool" -> Field fn "bool"
bool = B8

i8 :: Ground "i8" -> Field fn "i8"
i8 = I8

u8 :: Ground "u8" -> Field fn "u8"
u8 = U8

i16 :: Ground "i16" -> Field fn "i16"
i16 = I16

u16 :: Ground "u16" -> Field fn "u16"
u16 = U16

i32 :: Ground "i32" -> Field fn "i32"
i32 = I32

u32 :: Ground "u32" -> Field fn "u32"
u32 = U32

f32 :: Ground "f32" -> Field fn "f32"
f32 = F32

i64 :: Ground "i64" -> Field fn "i64"
i64 = I64

u64 :: Ground "u64" -> Field fn "u64"
u64 = U64

f64 :: Ground "f64" -> Field fn "f64"
f64 = F64

-- | write to file handlers
class Write (e :: Symbol) (t :: Symbol) where
  write :: (MonadIO m) => Handle -> Field fn t -> (Builder -> m r) -> m r

write' :: (MonadIO m) => Handle -> (Builder -> m r) -> Builder -> m r
write' h fn b = do
  r <- fn b
  liftIO $ B.hPutBuilder h b
  return r

instance Write "le" "bool" where
  write h (B8 b) fn = (B.word8 >>> write' h fn) (if b then 1 else 0)

instance Write "be" "bool" where
  write = write @"le" @"bool"

instance Write "le" "u8" where
  write h (U8 b) fn = (B.word8 >>> write' h fn) b

instance Write "be" "u8" where
  write = write @"le" @"u8"

instance Write "le" "i8" where
  write h (I8 b) fn = (B.char8 >>> write' h fn) b

instance Write "be" "i8" where
  write = write @"le" @"i8"

instance Write "le" "i16" where
  write h (I16 b) fn = (B.int16LE >>> write' h fn) b

instance Write "be" "i16" where
  write h (I16 b) fn = (B.int16BE >>> write' h fn) b

instance Write "le" "u16" where
  write h (U16 b) fn = (B.word16LE >>> write' h fn) b

instance Write "be" "u16" where
  write h (U16 b) fn = (B.word16BE >>> write' h fn) b

instance Write "le" "i32" where
  write h (I32 b) fn = (B.int32LE >>> write' h fn) b

instance Write "be" "i32" where
  write h (I32 b) fn = (B.int32BE >>> write' h fn) b

instance Write "le" "u32" where
  write h (U32 b) fn = (B.word32LE >>> write' h fn) b

instance Write "be" "u32" where
  write h (U32 b) fn = (B.word32BE >>> write' h fn) b

instance Write "le" "f32" where
  write h (F32 b) fn = (B.floatLE >>> write' h fn) b

instance Write "be" "f32" where
  write h (F32 b) fn = (B.floatBE >>> write' h fn) b

instance Write "le" "i64" where
  write h (I64 b) fn = (B.int64LE >>> write' h fn) b

instance Write "be" "i64" where
  write h (I64 b) fn = (B.int64BE >>> write' h fn) b

instance Write "le" "u64" where
  write h (U64 b) fn = (B.word64LE >>> write' h fn) b

instance Write "be" "u64" where
  write h (U64 b) fn = (B.word64BE >>> write' h fn) b

instance Write "le" "f64" where
  write h (F64 b) fn = (B.doubleLE >>> write' h fn) b

instance Write "be" "f64" where
  write h (F64 b) fn = (B.doubleBE >>> write' h fn) b
