{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Run (run) where

import Import
import Types.Prim as Prim
import Data.String (IsString(..))
import RIO.ByteString as B (hPut, replicate, unpack)
import Data.ByteString.Builder as B
import qualified Data.ByteString.Lazy as BL
import GHC.IO.Handle (hGetPosn)
import GHC.TypeLits
import Control.Monad (unless)
import Numeric (showIntAtBase)
import Data.Char (intToDigit)

--
import Data.Kind as K
data E where
  Foo :: E
  Bar :: E

class Fun (e :: E) where
  type family Fn e :: K.Type
  fun :: Fn e

instance Fun Foo where
  type instance Fn Foo = Int -> Int -> Maybe Int
  fun _ y = Just y

instance Fun Bar where
  type instance Fn Bar = String -> Maybe String
  fun = Just
--
data F where
  F :: { foo :: Int -> Int -> Maybe Int } -> F
  B :: { bar :: String -> Maybe String  } -> F

str f = case f of
  F{} -> show $ foo f 1 2
  B{} -> show $ bar f "poor one"
--

--
-- identity
data Id a where
  Id :: { unId :: forall r .
                  (a -> r)
               -> r
        } -> Id a
instance Functor Id where
  fmap f ida = Id \k -> unId ida (k . f)
instance Applicative Id where
  pure a = Id \k -> k a
  idf <*> ida = Id \k -> unId ida $ unId idf \f' -> k . f'
instance Monad Id where
  return = pure
  ida >>= f = Id \k -> unId ida \a -> unId (f a) k
instance (Show a) => Show (Id a) where
  show a = unId a show
-- maybe (Maybe)
data Mab a where
  Mab :: { unMab :: forall r .
                    r
                 -> (a -> r)
                 -> r
         } -> Mab a
instance Functor Mab where
  fmap f ma = Mab \n j -> unMab ma n (j . f)
instance Applicative Mab where
  pure a = Mab (const \j -> j a)
  mf <*> ma = Mab \n j -> unMab mf n \f -> unMab ma n $ j . f
instance Monad Mab where
  return = pure
  ma >>= f = Mab \n j -> unMab ma n \ja -> unMab (f ja) n j
instance (Show a) => Show (Mab a) where
  show ma = unMab ma "<nothing>" show
nothing = Mab \n _ -> n
just x = Mab $ const \j -> j x
-- logic (ListT)
data Logic a where
  Logic :: { unLogic :: forall r .
                        r
                     -> (r -> a -> r)
                     -> r
           } -> Logic a
instance (Show a) => Show (Logic a) where
  show la = unLogic la "<nil>" \r a -> show a <> ":" <> r
instance Functor Logic where
  fmap f la = Logic \n c -> unLogic la n \ra -> c ra . f
instance Applicative Logic where
  pure a = Logic \n c -> c n a
  lf <*> la = Logic \n c -> unLogic lf n $ const \f -> unLogic la n $ const (c n . f)
instance Monad Logic where
  return = pure
  la >>= f = Logic \n c -> unLogic la n \ra a -> unLogic (f a) ra c

nil = Logic const
cons h lt = Logic \n c -> c (unLogic lt n c) h
append as bs = Logic \n c -> unLogic as (unLogic bs n c) c

ls :: _
ls = cons 2 $ cons 1 nil
--


data C where
  C :: { c :: Field "c" "i8"
       , i :: Field "i" "i32"
       , d :: Field "d" "f64"
       , b :: Field "b" "bool"
       } -> C
  deriving (Show)

aC = C { c = store 'c'
       , i = store 123
       , d = store 3.1426
       , b = store True
       }

dblog :: String -> RIO App ()
dblog = fromString >>> logInfo

run :: forall m . (m ~ RIO App) => m ()
run = do
  logInfo "We're inside the application!"
  withBinaryFile
    "b.dat"
    WriteMode
    \h -> do
      let
        showHandle = (liftIO . hGetPosn) h >>= (show >>> dblog)

        write' :: forall (e :: Symbol) (t :: Symbol) . (Write e t, Store t) => Ground t -> m ()
        write' fld = do
          p <- hTell h
          let
            (_, al, _) = exec 0 (addfield @t)
            pad = align (fromIntegral p) al - (fromIntegral p)

          unless (pad == 0) do
            let
              payload = B.replicate pad 0
            dblog $ "padding " <> (show . B.unpack $ payload)
            hPut h payload
            showHandle
          write @e @t h (store fld) ( B.toLazyByteString
                                       >>> BL.unpack
                                       >>> fmap (flip (showIntAtBase 16 (intToDigit)) "")
                                       >>> show
                                       >>> dblog
                                      )
          showHandle

        act s m = do
          i <- fromIntegral <$> hTell h
          let
            (sz, al, _) = exec 0 m
            title = dblog ("@" <> show i <> " => " <> s <> ":" <> show sz <> ":" <> show al)
            footer = do
              p <- hTell h
              dblog ("@" <> show p <> " <= " <> s <> ":" <> show sz <> ":" <> show al)

            -- seek = fromIntegral >>> (hSeek h RelativeSeek)
            _write v = do
              let
                pad = align i al - i
              unless (0 == pad) do
                let
                  payload = B.replicate pad 0
                hPut h payload
                p <- hTell h
                dblog $ "pad " <> (show . unpack $ payload) <> " => @" <> show p

              dblog $ "write " <> (show . unpack $ v)
              hPut h v

          showHandle
          title
          _write (B.replicate sz 0)
          footer
          showHandle

      act "char"   (addfield @"i8")
      act "bool"   (addfield @"bool")
      act "byte"   (addfield @"u8")
      act "int"    (addfield @"i32")
      act "double" (addfield @"f64")

      act
        "{ c:char, i:int}"
        do
          addfield @"i8"
          addfield @"i32"

      act
        "{ c:char, i:int, c:char}"
        $ struct do
        addfield @"i8"
        addfield @"i32"
        addfield @"i8"

      act
        "struct M { char c; char c; int i; double d; }"
        $ struct do
            addfield @"i8"
            addfield @"i8"
            addfield @"i32"
            addfield @"f64"

      act
        "struct M' { double d; int i; char c; char c; }"
        $ struct do
            addfield @"f64"
            addfield @"i32"
            addfield @"i8"
            addfield @"i8"

      act
        "struct M'' { { char c; char c; int i; double d; } m; { double d; int i; char c; char c; } m'; }"
        $ struct do
            struct do
              addfield @"i8"
              addfield @"i8"
              addfield @"i32"
              addfield @"f64"
            struct do
              addfield @"f64"
              addfield @"i32"
              addfield @"i8"
              addfield @"i8"

      write' @"le" @"i8"  'c'
      write' @"le" @"i32" 16
      write' @"be" @"i32" 16
      write' @"le" @"f32" 16161616
      write' @"be" @"f32" 16161616
      write' @"le" @"f64" 16161616161616
      write' @"be" @"f64" 16161616161616

      dblog $ show $ fun @Foo 1 2
      dblog $ show $ fun @Bar "omf"

      dblog $ show $ str (F \_ y -> Just y)
      dblog $ show $ str (B Just)
      dblog $ show $ aC
      dblog $ show $ fmap (+2) ls
  logInfo "We're quiting the application!"
